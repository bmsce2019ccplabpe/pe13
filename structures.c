#include<stdio.h>
struct employee
{
  char name[20];
  char id[20];
  char dob[10];
  char add[50];
  int salary;
};

int
main ()
{
  int i;
  struct employee e;
  for (i = 0; i < 5; i++)
    {
      printf ("Enter employee details below for employee %d : \n",i+1);
      printf (" Name ");
      gets (e.name);
      printf (" Employee ID ");
      gets (e.id);
      printf (" DOB ");
      gets (e.dob);
      printf (" Address ");
      gets (e.add);
      printf (" Salary ");
      scanf ("%d", &e.salary);
      printf ("\n");
    }
  for (i = 0; i < 5; i++)
    {
      printf ("The employee details are as follows: \n");
      printf (" Name: ");
      puts (e.name);
      printf (" Employee ID: ");
      puts (e.id);
      printf (" DOB: ");
      puts (e.dob);
      printf (" Address: ");
      puts (e.add);
      printf (" Salary: %d \n", e.salary);
    }
  return 0;
}