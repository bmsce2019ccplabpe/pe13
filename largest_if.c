#include<stdio.h>
int largestnum (int, int, int);
int output (int, int, int, int);
int input ()
{
  int a;
  printf ("Enter a number \n");
  scanf ("%d", &a);
  return a;
}

int main ()
{
  int a, b, c, res;
  a = input ();
  b = input ();
  c = input ();
  res = largestnum (a, b, c);
  output (res, a, b, c);
  return 0;
}

int largestnum (int a, int b, int c)
{
  if (a > b && a > c)
    return a;
  else if (b > a && b > c)
    return b;
  else
    return c;
}

int output (int res, int a, int b, int c)
{
  printf ("The largest of %d, %d and %d is = %d \n", a, b, c, res);
  return 0;
}