#include<stdio.h>
int
main ()
{
  int a[20], n, small_pos, large_pos, small, large, i, temp;
  printf ("Enter the size of the array \n");
  scanf ("%d", &n);
  printf ("Start entering elements of the array \n");
  for (i = 0; i < n; i++)
    {
      scanf ("%d", &a[i]);
    }
  small = a[0];
  large = a[0];
  small_pos = 0;
  large_pos = 0;
  for (i = 0; i < n; i++)
    {
      if (a[i] < small)
	{
	  small = a[i];
	  small_pos = i;
	}
      if (a[i] > large)
	{
	  large = a[i];
	  large_pos = i;
	}
    }
  printf ("The smallest number in the array is %d at position %d \n", small,
	  small_pos);
  printf ("The largest number in the array is %d at position %d \n", large,
	  large_pos);
  temp = a[small_pos];
  a[small_pos] = a[large_pos];
  a[large_pos] = temp;
  printf ("The final array is displayed below \n");
  for (i = 0; i < n; i++)
    {
      printf ("%d \n", a[i]);
    }
  return 0;
}