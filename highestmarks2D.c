#include<stdio.h>
int
main ()
{
  int marks[5][3], i, j, max_marks;
  printf ("Enter the Marks of all 5 students below in all the 3 subjects \n");
  for (i = 0; i < 5; i++)
    {
      for (j = 0; j < 3; j++)
	{
	  scanf ("%d", &marks[i][j]);
	}
    }
  for (j = 0; j < 3; j++)
    {
      max_marks = marks[0][j];
      for (i = 0; i < 5; i++)
	{
	  if (marks[i][j] > max_marks)
	    max_marks = marks[i][j];
	}
      printf ("Maximum mark in subject %d is = %d \n", j, max_marks);
    }
  return 0;
}