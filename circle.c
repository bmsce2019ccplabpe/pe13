#include<stdio.h>
#include<math.h>
#define pi 3.14
float input()
{
	float r;
	printf("Enter the radius of circle\n");
	scanf("%f", &r);
	return r;
}

float compute_area(float r)
{
	float ar;
	ar = pi * r * r;
	return ar;
}

float output_area(float a)
{
	printf("\n The area of the circle is = %f", a);
}

float compute_circum(float r)
{
	float cir;
	cir = 2 * pi * r;
	return cir;
}

float output_circumf(float c)
{
	printf("\n The circumference of the circle is = %f \n", c);
}

int main()
{ 
	float area, circumference, radius;
	radius = input();
	area = compute_area(radius);
	output_area(area);
	circumference = compute_circum(radius);
	output_circumf(circumference);
	return 0;
}