#include<stdio.h>
int
main ()
{
  int a[10], n, key, i, j;
  int flag = 0;
  printf ("Enter size of the array \n");
  scanf ("%d", &n);
  printf ("Enter array elements \n");
  for (i = 0; i < n; i++)
    scanf ("%d", &a[i]);
  printf ("Enter key value to be searched in array \n");
  scanf ("%d", &key);
  for (i = 0; i < n; i++)
    {
      if (a[i] == key)
	{
	  flag = 1;
	  printf ("Element %d found at position %d \n", a[i], i);
	  break;
	}
    }
  if (flag = 0)
    printf ("Element %d not found in the array \n", key);
  return 0;
}